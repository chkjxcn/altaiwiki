#!/bin/awk -f

#freemem(K) | cpu idle(%) | radio0 rx | radio0 tx | radio1 rx | radio1 tx (Mbps)
BEGIN{
    interval = 5;
    if(ARGV[1]) interval = ARGV[1];
    ocpu = 0, ncpu = 0, dcpu = 0, pec = 0.0;
    oidle = 0, nidle = 0, didle = 0;  # %idle = (newidle - oldidle)/(newcpu - newcpu)
    free = 0;
    radio0rx = 0, radio0tx = 0, radio1rx = 0, radio1tx = 0;

    while(getline < "/proc/stat") {  #get init value
            if($0 ~ /cpu /) {
                #user nice system idle io irq sirq 
                ncpu = $2 + $3 + $4 + $5 + $6 + $7 + $8;
                nidle = $5;
            }
        }
        close("/proc/stat");

    while(system("trap 'exit 0' 2; sleep "interval";exit 1")) {
        while(getline < "/proc/meminfo") {  #print free mem
            if($0 ~ /MemFree/) {
                printf($2" ");    
            }
        }
        close("/proc/meminfo");
        while(getline < "/proc/stat") {  #print idle
            if($0 ~ /cpu /) {
                #user nice system idle io irq sirq 
                ncpu = $2 + $3 + $4 + $5 + $6 + $7 + $8;
                nidle = $5;
                dcpu = ncpu - ocpu;
                didle = nidle - oidle;
                if (dcpu == 0)
                    pec = 0;
                else
                    pec = didle/dcpu;
                printf("%.2f ", pec * 100);
            }
        }
        close("/proc/stat");
        radio0rx = 0, radio0tx = 0, radio1rx = 0, radio1tx = 0;
        while(getline < "/etc/config_external/status_interface") {
            if($0 ~ /interface_radio0_0/) {
                while(getline < "/etc/config_external/status_interface") {
                    if($0 ~ /tx_bps/) {
                        gsub(/'/, "", $3); radio0tx = $3;}
                    if($0 ~ /rx_bps/) {
                        gsub(/'/, "", $3); radio0rx = $3;}
                    if($0 ~ /^[ \t]*$/)
                        break;
                }
            }
            if($0 ~ /interface_radio1_0/) {
                while(getline < "/etc/config_external/status_interface") {
                    if($0 ~ /tx_bps/) {
                        gsub(/'/, "", $3); radio1tx = $3;}
                    if($0 ~ /rx_bps/) {
                        gsub(/'/, "", $3); radio1rx = $3;}
                    if($0 ~ /^[ \t]*$/)
                        break;
                }
            }
        }
        printf("%.3f %.3f %.3f %.3f\n", radio0rx/2**20,  radio0tx/2**20, radio1rx/2**20, radio1tx/2**20);
        close("/etc/config_external/status_interface");
                        
    }
}
