<<TableOfContents()>>

== Part 1: Physical Page Management ==
这部分主要是内存管理

 * boot_alloc()
 * mem_init()
 * page_init()
 * page_alloc()
 * page_free()

按照代码里的注释就可以轻松完成

== Part 2: Virtual Memory ==
 * pgdir_walk()
 * boot_map_region()
 * page_lookup()
 * page_remove()
 * page_insert()

需要注意的是page_insert:

 . 之前实现的时候会先查看一下原来存储的pte值,但是现在的注释特别强调有更好的方法.
   {{{#!highlight c
// Corner-case hint: Make sure to consider what happens when the same
// pp is re-inserted at the same virtual address in the same pgdir.
// Don't be tempted to write special-case code to handle this
// situation, though; there's an elegant way to address it.
int
page_insert(pde_t *pgdir, struct Page *pp, void *va, int perm)
{
        // Fill this function in
        pte_t *p;
        physaddr_t ppa = page2pa(pp);

#if defined (DEBUG_LAB2)
        cprintf("page_insert: pa: 0x%x va: 0x%x\n", page2pa(pp), va);
#endif
        ++pp->pp_ref;    //这个是关键,需要在插入之前就增加引用,因此如果该page被映射也不会被free掉,下面就可以不用考虑特殊情况
        p = pgdir_walk(pgdir, va, 1);
#if defined (DEBUG_LAB2)
        cprintf("page_insert: pte: 0x%x\n 0x%x", p, p && *p);
#endif
        if (p == NULL){
                --pp->pp_ref;
                return -E_NO_MEM;
        }
        //if PTE_ADDR(*p) != 0, then remove it.
        if (PTE_ADDR(*p) != 0){
                page_remove(pgdir, va);
                tlb_invalidate(pgdir, va);
        }
        *p |= page2pa(pp) | perm | PTE_P;
        return 0;
}

}}}

== Part 3: Kernel Address Space ==

 mem_init():
  . map系统vm空间只需要调用boot_map_region(),
  . 因为这些映射会在系统过程中一直保留,所以不需要加到内存管理中.
