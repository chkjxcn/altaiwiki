= Part 1: =

{{{
+------------------+  <- 0xFFFFFFFF (4GB)
|      32-bit      |
|  memory mapped   |
|     devices      |
|                  |
/\/\/\/\/\/\/\/\/\/\

/\/\/\/\/\/\/\/\/\/\
|                  |
|      Unused      |
|                  |
+------------------+  <- depends on amount of RAM
|                  |
|                  |
| Extended Memory  |
|                  |
|                  |
+------------------+  <- 0x00100000 (1MB)
|     BIOS ROM     |
+------------------+  <- 0x000F0000 (960KB)
|  16-bit devices, |
|  expansion ROMs  |
+------------------+  <- 0x000C0000 (768KB)
|   VGA Display    |
+------------------+  <- 0x000A0000 (640KB)
|                  |
|    Low Memory    |
|                  |
+------------------+  <- 0x00000000
}}}
== 启动过程 ==
 1. PC上电后CS:IP = 0xf000:0xfff0,属于BIOS地址,BIOS将boot loader加载到0x7c00 ~ 0x7dff,并且跳转到0x7c00
 1. boot 切换到保护模式并加载内核:
   1. 切换到保护模式
   {{{#!highlight gas
  # Switch from real to protected mode, using a bootstrap GDT
  # and segment translation that makes virtual addresses
  # identical to their physical addresses, so that the
  # effective memory map does not change during the switch.
  lgdt    gdtdesc
  movl    %cr0, %eax
  orl     $CR0_PE_ON, %eax
  movl    %eax, %cr0

  # Jump to next instruction, but in 32-bit code segment.
  # Switches processor into 32-bit mode.
  ljmp    $PROT_MODE_CSEG, $protcseg
}}}
   2. 从hd读取kernel
   {{{#!highlight c
void
bootmain(void)
{
        struct Proghdr *ph, *eph;

        // read 1st page off disk
        readseg((uint32_t) ELFHDR, SECTSIZE*8, 0);

        // is this a valid ELF?
        if (ELFHDR->e_magic != ELF_MAGIC)
                goto bad;

        // load each program segment (ignores ph flags)
        ph = (struct Proghdr *) ((uint8_t *) ELFHDR + ELFHDR->e_phoff);
        eph = ph + ELFHDR->e_phnum;
        for (; ph < eph; ph++)
                // p_pa is the load address of this segment (as well
                // as the physical address)
                readseg(ph->p_pa, ph->p_memsz, ph->p_offset);

        // call the entry point from the ELF header
        // note: does not return!
        ((void (*)(void)) (ELFHDR->e_entry))();

bad:
        outw(0x8A00, 0x8A00);
        outw(0x8A00, 0x8E00);
        while (1)
                /* do nothing */;
}
}}}
   注意objdump不会显示出全部的sector
   {{{
[chk@D ~/6.828/lab]$ readelf -l obj/kern/kernel

Elf file type is EXEC (Executable file)
Entry point 0x10000c
There are 2 program headers, starting at offset 52

Program Headers:
  Type           Offset   VirtAddr   PhysAddr   FileSiz MemSiz  Flg Align
  LOAD           0x001000 0xf0100000 0x00100000 0x0d5b1 0x0d5b1 R E 0x1000
  LOAD           0x00f000 0xf010e000 0x0010e000 0x0a300 0x0a960 RW  0x1000

 Section to Segment mapping:
  Segment Sections...
   00     .text .rodata .stab .stabstr
   01     .data .bss
}}}
   实际上sector有一些不会加载,如下图所示,

   所有的Addr=0的都不会加载到内存中,因为根本没有加载的物理地址
   {{{
[chk@D ~/6.828/lab]$ readelf -S obj/kern/kernel
There are 11 section headers, starting at offset 0x19360:

Section Headers:
  [Nr] Name              Type            Addr     Off    Size   ES Flg Lk Inf Al
  [ 0]                   NULL            00000000 000000 000000 00      0   0  0
  [ 1] .text             PROGBITS        f0100000 001000 001790 00  AX  0   0  4
  [ 2] .rodata           PROGBITS        f01017a0 0027a0 0006dc 00   A  0   0 32
  [ 3] .stab             PROGBITS        f0101e7c 002e7c 0043f9 0c   A  4   0  4
  [ 4] .stabstr          STRTAB          f0106275 007275 00733c 00   A  0   0  1
  [ 5] .data             PROGBITS        f010e000 00f000 00a300 00  WA  0   0 4096
  [ 6] .bss              NOBITS          f0118300 019300 000660 00  WA  0   0 32
  [ 7] .comment          PROGBITS        00000000 019300 000011 01  MS  0   0  1
  [ 8] .shstrtab         STRTAB          00000000 019311 00004c 00      0   0  1
  [ 9] .symtab           SYMTAB          00000000 019518 000670 10     10  47  4
  [10] .strtab           STRTAB          00000000 019b88 000373 00      0   0  1
Key to Flags:
  W (write), A (alloc), X (execute), M (merge), S (strings)
  I (info), L (link order), G (group), x (unknown)
  O (extra OS processing required) o (OS specific), p (processor specific)
}}}
