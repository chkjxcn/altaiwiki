== 简介 ==
tmux是一款非常好用的终端程序，
一般来说，安装只需要
 1. freebsd: pkg_add -r tmux
 1. debian or other linux use deb: apt-get install tmux

{{attachment:sample.png|截图|width=800 height=400}}

现在的版本是1.5

== 配置文件 ==
请把附件中的.tmux.conf置于用户的HOME下：
{{{
set-option -g history-limit 20000
set-option -g base-index 1 
set-option -g status-keys vi   
set-option -g status-right "#(date +%H:%M' ')" 
set-option -g status-utf8 on
set-option -g status-right-length 10 
set-window-option -g mode-keys vi
#set -g default-terminal "xterm-256color"

#上面是一些基本配置

set-option -g prefix C-x
unbind-key C-b
bind-key C-x send-prefix
bind-key k kill-session
bind-key h split-window -h
bind-key v split-window -v

#上面做键绑定

set -g status-bg '#333333'
setw -g window-status-current-fg '#FFFFFF'
setw -g window-status-fg '#AAAAAA'
setw -g status-right-fg "#00cc99"
setw -g window-status-current-attr bright,bold  
setw -g pane-active-border-fg 'colour22'
set-option -g mode-mouse on
set-option -g mouse-resize-pane on
set-option -g mouse-select-pane on 
set-option -g mouse-select-window on

#上面设置颜色以及开启鼠标模式

unbind ^C
bind ^C new-window

unbind ^X
bind -r ^X next-window

unbind ^D
bind ^D detach

unbind x
bind x list-clients

unbind ^E
bind ^E last-window

unbind ^W
bind ^W list-windows

#unbind ^Z
#bind -r ^Z 

unbind a
bind -r a resizep -L
unbind d
bind -r d resizep -R
unbind s
bind -r s resizep -D
unbind w
bind -r w resizep -U
}}}

== 复制模式的说明 ==
{{{

tmux复制模式的命令表：

       Function                        vi             emacs
       Back to indentation             ^              M-m
       Clear selection                 Escape         C-g
       Copy selection                  Enter          M-w
       Cursor down                     j              Down
       Cursor left                     h              Left
       Cursor right                    l              Right
       Cursor to bottom line           L
       Cursor to middle line           M              M-r
       Cursor to top line              H              M-R
       Cursor up                       k              Up
       Delete entire line              d              C-u
       Delete to end of line           D              C-k
       End of line                     $              C-e
       Goto line                       :              g
       Half page down                  C-d            M-Down
       Half page up                    C-u            M-Up
       Next page                       C-f            Page down
       Next word                       w              M-f
       Paste buffer                    p              C-y
       Previous page                   C-b            Page up
       Previous word                   b              M-b
       Quit mode                       q              Escape
       Scroll down                     C-Down or J    C-Down
       Scroll up                       C-Up or K      C-Up
       Search again                    n              n
       Search backward                 ?              C-r
       Search forward                  /              C-s
       Start of line                   0              C-a
       Start selection                 Space          C-Space
       Transpose chars                 C-t

复制模式步骤：
我的控制键为：C-x (这个看自己绑定的是哪个键）
1. C-x [ 进入复制模式
2. 参考上表移动鼠标到要复制的区域，移动鼠标时可用vim的搜索功能"/","?"
3. 安空格键开始选择复制区域
4. 选择完成后安enter键退出
5. C-x ] 粘贴
}}}
